-------------------------------------------------------------------------------
                            Phone Number Field
-------------------------------------------------------------------------------
The Phone Number field module provides a simple field that allows you to add a
national or international phone number to a content type and it validate the 
phone number using Byteplant API (http://www.phone-validator.net/)


Author
------
Nitin Kumar Singh
nitin@incaendo.com
