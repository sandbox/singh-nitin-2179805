<?php

/**
 * @file
 * Defines the admin features for Phone number field module.
 */

/**
 * Administrator form to set the api key.
 */
function phone_number_field_admin($form, &$form_state) {
  if (!variable_get('phone_number_field_APIKey', '')) {
    drupal_set_message(t('Register yourself to !url to get the API key.',
      array('!url' => '<a href="http://www.phone-validator.net/phone-number-online-validation-api.html" target="_blank">Byte plant</a>')));
  }
  $form = array();
  $form['phone_number_field_APIKey'] = array(
    '#type' => 'textfield',
    '#title' => t('API Key'),
    '#default_value' => variable_get('phone_number_field_APIKey', ''),
  );
  return system_settings_form($form);
}
